# AirHocky

3D simulation of an air hocky game using java and java3D.



# System Architecture

![system architecture](https://raw.githubusercontent.com/bhlshrf/AirHocky/master/sysArc.jpg)



# Demo

![demo](https://raw.githubusercontent.com/bhlshrf/AirHocky/master/demo.gif)



# More Info

 you may want take a look at the presentation file 'AirHockey AI Prez.ppsx'



 
